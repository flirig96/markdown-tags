# markdown-tags

Shows tags for markdown files in Tree View and allows navigation to files with
the selected tag.

The Project can be found on GitLab: https://gitlab.com/SimVet/markdown-tags

If you have any Issues or Feature suggestions create an issue
[here](https://gitlab.com/SimVet/markdown-tags/issues).
